package HomeTwo;

public class LoginPage extends Page implements IChrome, IFirefox {
	
	public LoginPage(String title, String url, String htmlContent) {
		super(title, url, htmlContent);
	}
	
	@Override
	public void load() {
		System.out.println("Login Page is loaded");
		
	}

	@Override
	public void refresh() {
		System.out.println("Login Page is refreshed");
		
	}

	@Override
	public void openInChrome() {
		System.out.println("Login Page is opened in Chrome");
		
	}

	@Override
	public void openInFirefox() {
		System.out.println("Login Page is opened in Chrome");
		
	}
	
	@Override
	public String htmlBuilder() {
		return ("Login Page has the following content: " + getHtmlContent());
	}
}
