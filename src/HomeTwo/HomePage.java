package HomeTwo;

public class HomePage extends Page implements IChrome,IFirefox {
	
	public HomePage(String title, String url, String htmlContent) {
		super(title, url, htmlContent);
	}
	
	@Override
	public void load() {
		System.out.println("Home Page is loaded");
		
	}

	@Override
	public void refresh() {
		System.out.println("Home Page is refreshed");
		
	}

	@Override
	public void openInChrome() {
		System.out.println("Home Page is opened in Chrome");
		
	}

	@Override
	public void openInFirefox() {
		System.out.println("Home Page is opened in Chrome");
		
	}
	
	@Override
	public String htmlBuilder() {
		return ("Home Page has the following content: " + getHtmlContent());
	}
}
