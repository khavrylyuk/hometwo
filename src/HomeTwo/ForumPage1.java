package HomeTwo;

public class ForumPage1 extends Page implements IChrome {
	
	public ForumPage1(String title, String url, String htmlContent) {
		super(title, url, htmlContent);
	}
	
	@Override
	public void load() {
		System.out.println("Forum Page #1 is loaded");
		
	}

	@Override
	public void refresh() {
		System.out.println("Forum Page #1 is refreshed");
		
	}

	@Override
	public void openInChrome() {
		System.out.println("Forum Page #1 is opened in Chrome");
		
	}


	@Override
	public String htmlBuilder() {
		return ("Forum Page #1 has the following content: " + getHtmlContent());
	}
}
