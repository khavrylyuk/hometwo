package HomeTwo;

public class AboutPage2 extends Page implements IFirefox {
	
	public AboutPage2(String title, String url, String htmlContent) {
		super(title, url, htmlContent);
	}
	
	@Override
	public void load() {
		System.out.println("About page #2 is loaded");
		
	}

	@Override
	public void refresh() {
		System.out.println("About page #2 is refreshed");
		
	}

	@Override
	public void openInFirefox() {
		System.out.println("About page #2 is opened in Firefox");
		
	}


	@Override
	public String htmlBuilder() {
		return ("AboutPage #2 has the following content: " + getHtmlContent());
	}
}
