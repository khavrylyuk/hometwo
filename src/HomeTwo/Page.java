package HomeTwo;

public abstract class Page  {
	private String title;
	private String url;
	private String htmlContent;
	
	
	public Page(String title, String url, String htmlContent) {
		this.title = title;
		this.url = url;
		this.htmlContent = htmlContent;
	}
	
	public String toString() {
	return getClass().getSimpleName();
		}
	
	public abstract void load();
	
	public abstract void refresh();
		
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHtmlContent() {
		return htmlContent;
	}
	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}
	
	public abstract String htmlBuilder();
	
}
