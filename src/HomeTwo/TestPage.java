package HomeTwo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class TestPage {

	public static void main(String[] args) {

		Page page1 = new AboutPage1("About", "www.site.com/about1", "About1_Content");
		Page page2 = new AboutPage2("About", "www.site.com/about2", "About2_Content");
		Page page3 = new ForumPage1("Forum", "www.site.com/forum1", "Forum1_Content");
		Page page4 = new ForumPage2("Forum", "www.site.com/forum2", "Forum2_Content");
		Page page5 = new HomePage("Home", "www.site.com/home", "Home_Content");
		Page page6 = new LoginPage("Login", "www.site.com/login", "Login_Content");
		
		ArrayList<Page> listOfPages = new ArrayList<>();
		listOfPages.add(page1);
		listOfPages.add(page2);
		listOfPages.add(page3);
		listOfPages.add(page4);
		listOfPages.add(page5);
		listOfPages.add(page6);
		
		System.out.println(listOfPages);
		
		System.out.println("-------------------------------------------------------------------");
		
		System.out.println("The following pages are opened in Chrome:");
		for (Page page : listOfPages) {
			
			List<Page> ChromeList = new ArrayList<>();
			if (page instanceof IChrome) {
				ChromeList.add(page);
				System.out.println("- "+ ChromeList.toString());
				}
		}
		
		System.out.println("The following pages are opened in Firefox:");
		for (Page page : listOfPages) {	
			List<Page> FirefoxList = new ArrayList<>();
			if (page instanceof IFirefox) {
				FirefoxList.add(page);
				System.out.println("- " + FirefoxList.toString());
				}
			}
		
		System.out.println("-------------------------------------------------------------------");
		
		listOfPages.forEach(a -> System.out.println(a.htmlBuilder()));
		
		System.out.println("-------------------------------------------------------------------");
		
		HashMap<String, ArrayList<Page>> mapOfPages = new HashMap<>();
		for (Page page : listOfPages) {
			if (!mapOfPages.containsKey(page.getTitle())) {
				mapOfPages.put(page.getTitle(), new ArrayList<>());
			}
			mapOfPages.get(page.getTitle()).add(page);
 		}
		System.out.println(mapOfPages);
		
		System.out.println("-------------------------------------------------------------------");
		
		for (Entry<String, ArrayList<Page>> entry: mapOfPages.entrySet()) {
			for (Page page : entry.getValue()) {
				System.out.println(page.getHtmlContent());
			}
		}
		
	}
	
	
}
