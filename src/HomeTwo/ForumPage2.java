package HomeTwo;

public class ForumPage2 extends Page implements IChrome {
	
	public ForumPage2(String title, String url, String htmlContent) {
		super(title, url, htmlContent);
	}
	
	@Override
	public void load() {
		System.out.println("Forum Page #2 is loaded");
		
	}

	@Override
	public void refresh() {
		System.out.println("Forum Page #2 is refreshed");
		
	}

	@Override
	public void openInChrome() {
		System.out.println("Forum Page #2 is opened in Chrome");
		
	}

	@Override
	public String htmlBuilder() {
		return ("Forum Page #2 has the following content: " + getHtmlContent());
	}
}
