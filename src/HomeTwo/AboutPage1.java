package HomeTwo;

public class AboutPage1 extends Page implements IFirefox {

	public AboutPage1(String title, String url, String htmlContent) {
		super(title, url, htmlContent);
	}
	
	@Override
	public void load() {
		System.out.println("About page #1 is loaded");
		
	}

	@Override
	public void refresh() {
		System.out.println("About page #1 is refreshed");
		
	}

	@Override
	public void openInFirefox() {
		System.out.println("About page #1 is opened in Firefox");
		
	}


	@Override
	public String htmlBuilder() {
		return ("AboutPage #1 has the following content: " + getHtmlContent());
	}
	
	
	
	
}
